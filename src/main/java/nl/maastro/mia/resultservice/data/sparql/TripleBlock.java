package nl.maastro.mia.resultservice.data.sparql;

import java.util.List;

public class TripleBlock {
	List<Triple> triples;
	
	@Override
	public String toString(){
		StringBuilder tripleBlockBuilder = new StringBuilder();
		for(Triple triple : triples){
			tripleBlockBuilder.append(triple + System.lineSeparator());
		}
		return tripleBlockBuilder.toString();
	}

	public List<Triple> getTriples() {
		return triples;
	}

	public void setTriples(List<Triple> triples) {
		this.triples = triples;
	}
}