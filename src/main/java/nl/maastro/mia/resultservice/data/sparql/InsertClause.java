package nl.maastro.mia.resultservice.data.sparql;

import java.util.List;

public class InsertClause {
	private final TripleBlock tripleBlock = new TripleBlock();
	
	public InsertClause(List<Triple> triples){
		tripleBlock.setTriples(triples);
	}
	
	@Override
	public String toString(){
		StringBuilder insertBuilder = new StringBuilder();
		insertBuilder.append("INSERT{" + System.lineSeparator());
		insertBuilder.append(tripleBlock);
		insertBuilder.append("}" + System.lineSeparator());
		
		return insertBuilder.toString();
	}
}