package nl.maastro.mia.resultservice.data.sparql;

import java.util.ArrayList;
import java.util.List;

public class PrefixBlock {
	private final List<Prefix> prefixes = new ArrayList<>();

	public List<Prefix> getPrefixes() {
		return prefixes;
	}

	public void setPrefixes(List<Prefix> prefixes) {
		this.prefixes.clear();
		this.prefixes.addAll(prefixes);
	}
	
	@Override
	public String toString(){
		StringBuilder prefixBlockBuilder = new StringBuilder();
		for(Prefix prefix : prefixes){
			prefixBlockBuilder.append(prefix + System.lineSeparator());
		}
		
		return prefixBlockBuilder.toString();
	}
}