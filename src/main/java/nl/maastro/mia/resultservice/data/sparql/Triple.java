package nl.maastro.mia.resultservice.data.sparql;

public class Triple {
	String subject;
	String predicate;
	String object;

	public Triple(String subject, String predicate, String object) {
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
	}

	@Override
	public String toString(){
		StringBuilder tripleBuilder = new StringBuilder();
		tripleBuilder.append(subject);
		tripleBuilder.append(" ");
		tripleBuilder.append(predicate);
		tripleBuilder.append(" ");
		tripleBuilder.append(object);
		tripleBuilder.append(".");
		return tripleBuilder.toString();
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getPredicate() {
		return predicate;
	}

	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}
}