package nl.maastro.mia.resultservice.data.sparql;

import java.util.List;

public class WhereClause {
	private final TripleBlock tripleBlock = new TripleBlock();
	
	public WhereClause(List<Triple> triples){
		tripleBlock.setTriples(triples);
	}
	
	@Override
	public String toString(){
		StringBuilder insertBuilder = new StringBuilder();
		insertBuilder.append("WHERE");
		insertBuilder.append("{" + System.lineSeparator());
		insertBuilder.append(tripleBlock);
		insertBuilder.append("}" + System.lineSeparator());
		
		return insertBuilder.toString();
	}
}