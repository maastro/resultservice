package nl.maastro.mia.resultservice.data.sparql;

public class SparqlInsert {
	PrefixBlock prefixBlock;
	InsertClause insertClause;
	WhereClause whereClause;
	
	@Override
	public String toString(){
		StringBuilder insertBuilder = new StringBuilder();
		insertBuilder.append(prefixBlock);
		insertBuilder.append(System.lineSeparator());
		insertBuilder.append(insertClause + System.lineSeparator());
		insertBuilder.append(System.lineSeparator());
		insertBuilder.append(whereClause + System.lineSeparator());
		
		return insertBuilder.toString();
	}
	
	public InsertClause getInsertClause() {
		return insertClause;
	}

	public void setInsertClause(InsertClause insertClause) {
		this.insertClause = insertClause;
	}

	public WhereClause getWhereClause() {
		return whereClause;
	}

	public void setWhereClause(WhereClause whereClause) {
		this.whereClause = whereClause;
	}

	public PrefixBlock getPrefixBlock() {
		return prefixBlock;
	}

	public void setPrefixBlock(PrefixBlock prefixBlock) {
		this.prefixBlock = prefixBlock;
	}
}