package nl.maastro.mia.resultservice.data.sparql;

public class TripleElement {
	String location;
	String data;
	
	public TripleElement(String location, String data){
		this.location = location;
		this.data = data;
	}

	@Override
	public String toString(){
		StringBuilder tripleElementBuilder = new StringBuilder();
		tripleElementBuilder.append(location);
		tripleElementBuilder.append(":");
		tripleElementBuilder.append(data);

		return tripleElementBuilder.toString();
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	} 
}