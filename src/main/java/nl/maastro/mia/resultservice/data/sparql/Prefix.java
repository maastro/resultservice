package nl.maastro.mia.resultservice.data.sparql;

public class Prefix {
	String name;
	String resourceLocation;
	
	public Prefix(String name, String resourceLocation){
		this.name = name;
		this.resourceLocation = resourceLocation;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getResourceLocation() {
		return resourceLocation;
	}
	public void setResourceLocation(String resourceLocation) {
		this.resourceLocation = resourceLocation;
	}
	
	@Override
	public String toString(){
		return "PREFIX " + name + ":" + "<" + resourceLocation + ">";
	}
}