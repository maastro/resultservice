package nl.maastro.mia.resultservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.mia.resultservice.entity.DoseResult;

@Repository
public interface DoseResultRepository extends JpaRepository<DoseResult, Long>{
	
	Page<DoseResult> findByPatientId(String patientId, Pageable pageable);

}
