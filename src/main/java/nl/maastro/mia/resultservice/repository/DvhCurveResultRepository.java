package nl.maastro.mia.resultservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.mia.resultservice.entity.DvhCurveResult;

@Repository
public interface DvhCurveResultRepository extends JpaRepository<DvhCurveResult, Long>{

	Page<DvhCurveResult	> findByPatientId(String query, Pageable pageable);

}
