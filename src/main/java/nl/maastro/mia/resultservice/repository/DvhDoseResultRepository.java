package nl.maastro.mia.resultservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.mia.resultservice.entity.DvhDoseResult;

@Repository
public interface DvhDoseResultRepository extends JpaRepository<DvhDoseResult, Long>{

	Page<DvhDoseResult> findByPatientId(String query, Pageable pageable);

}
