package nl.maastro.mia.resultservice.repository.sparql;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import nl.maastro.mia.resultservice.config.RdfConfiguration;

@Repository
@EnableConfigurationProperties(RdfConfiguration.class)
public class SparqlResultRepository {

	private static final String dataFormParameterUpdate = "update";

	private static final Logger LOGGER = Logger.getLogger(SparqlResultRepository.class);

	@Autowired
	private RdfConfiguration rdfConfiguration;
	
	public boolean updateQuery(String query){
		
		String url = rdfConfiguration.getEndpoint();
		
		if(query.isEmpty() || query.contains("null")){
			LOGGER.error("Query is null, contains null or is empty. Cannot proceed");
			LOGGER.error("Error Query : " + System.lineSeparator() + query);
			return false;
		}

		try{
			MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<String, String>();
			bodyMap.add(dataFormParameterUpdate, query);
			
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(bodyMap, headers);

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

			if(!response.getStatusCode().is2xxSuccessful()){
				
				String body = response.getBody();
				if(body!=null){
					LOGGER.error(body);
				}
				LOGGER.error("Error Query : " + System.lineSeparator() + query);
				return false;
			}

		} catch (Exception e) {
			LOGGER.error("Error Query : " + System.lineSeparator() + query, e);
			return false;
		}
		return true;
	}
	
}