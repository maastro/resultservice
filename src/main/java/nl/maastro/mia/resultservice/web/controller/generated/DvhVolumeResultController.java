package nl.maastro.mia.resultservice.web.controller.generated;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.resultservice.entity.DvhVolumeResult;
import nl.maastro.mia.resultservice.service.generated.DvhVolumeResultService;
import nl.maastro.mia.resultservice.web.controller.util.PaginationUtil;
import nl.maastro.mia.resultservice.web.dto.generated.DvhVolumeResultDto;
import nl.maastro.mia.resultservice.web.mapper.DvhVolumeResultMapper;

/**
 * REST controller for managing DvhVolumeResult.
 */
@RestController
@RequestMapping("/api")
public class DvhVolumeResultController {

    private final Logger log = LoggerFactory.getLogger(DvhVolumeResultController.class);
        
    @Autowired
    private DvhVolumeResultService dvhVolumeResultService;
    
    @Autowired
    private DvhVolumeResultMapper dvhVolumeResultMapper;
    
    /**
     * GET  /dvh-volume-results : get all the dvhVolumeResults.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dvhVolumeResults in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/dvh-volume-results",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional(readOnly = true)
    public ResponseEntity<List<DvhVolumeResultDto>> getAllDvhVolumeResults(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of DvhVolumeResults");
        Page<DvhVolumeResult> page = dvhVolumeResultService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dvh-volume-results");
        return new ResponseEntity<>(dvhVolumeResultMapper.map(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /dvh-volume-results/:id : get the "id" dvhVolumeResult.
     *
     * @param id the id of the dvhVolumeResultDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dvhVolumeResultDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/dvh-volume-results/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DvhVolumeResultDto> getDvhVolumeResult(@PathVariable Long id) {
        log.debug("REST request to get DvhVolumeResult : {}", id);
        DvhVolumeResultDto dvhVolumeResultDTO = dvhVolumeResultService.findOne(id);
        return Optional.ofNullable(dvhVolumeResultDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

	/**
	 * SEARCH  /_search/containers?query=:query : search for the container corresponding
	 * to the query.
	 *
	 * @param query the query of the container search
	 * @return the result of the search
	 */
	@RequestMapping(value = "/_search/dvh-volume-results",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<DvhVolumeResult>> search(@RequestParam String query, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to search for a page of Containers for query {} " + query);
		Page<DvhVolumeResult> page = dvhVolumeResultService.search(query, pageable);
		HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/dvh-volume-results");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

}
