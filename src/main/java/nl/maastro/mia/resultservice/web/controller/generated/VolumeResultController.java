package nl.maastro.mia.resultservice.web.controller.generated;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.resultservice.entity.VolumeResult;
import nl.maastro.mia.resultservice.service.generated.VolumeResultService;
import nl.maastro.mia.resultservice.web.controller.util.PaginationUtil;
import nl.maastro.mia.resultservice.web.dto.generated.VolumeResultDto;
import nl.maastro.mia.resultservice.web.mapper.VolumeResultMapper;

/**
 * REST controller for managing VolumeResult.
 */
@RestController
@RequestMapping("/api")
public class VolumeResultController {

    private final Logger log = LoggerFactory.getLogger(VolumeResultController.class);
        
    @Autowired
    private VolumeResultService volumeResultService;
    
    @Autowired
    private VolumeResultMapper volumeResultMapper;
    
    /**
     * GET  /volume-results : get all the volumeResults.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of volumeResults in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/volume-results",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional(readOnly = true)
    public ResponseEntity<List<VolumeResultDto>> getAllVolumeResults(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of VolumeResults");
        Page<VolumeResult> page = volumeResultService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/volume-results");
        return new ResponseEntity<>(volumeResultMapper.map(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /volume-results/:id : get the "id" volumeResult.
     *
     * @param id the id of the volumeResultDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the volumeResultDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/volume-results/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VolumeResultDto> getVolumeResult(@PathVariable Long id) {
        log.debug("REST request to get VolumeResult : {}", id);
        VolumeResultDto volumeResultDTO = volumeResultService.findOne(id);
        return Optional.ofNullable(volumeResultDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    
	/**
	 * SEARCH  /_search/containers?query=:query : search for the container corresponding
	 * to the query.
	 *
	 * @param query the query of the container search
	 * @return the result of the search
	 */
	@RequestMapping(value = "/_search/volume-results",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<VolumeResult>> search(@RequestParam String query, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to search for a page of Containers for query {} " + query);
		Page<VolumeResult> page = volumeResultService.search(query, pageable);
		HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/volume-results");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

}
