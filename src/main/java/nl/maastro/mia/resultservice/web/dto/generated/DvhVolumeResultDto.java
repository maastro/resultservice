package nl.maastro.mia.resultservice.web.dto.generated;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the DvhVolumeResult entity.
 */
public class DvhVolumeResultDto implements Serializable {

    private Long id;
    
    private String volumeOfInterest;
    private String doseLimit;
    private String doseUnit;
    private Float result;
    private String resultUnit;
    private String error;
    
    private String patientId;
    private String planLabel;
    private String doseSopUid;
    private String containerId;
    private String calculationIdentifier;
    
    private String version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVolumeOfInterest() {
		return volumeOfInterest;
	}

	public void setVolumeOfInterest(String volumeOfInterest) {
		this.volumeOfInterest = volumeOfInterest;
	}

	public String getDoseLimit() {
		return doseLimit;
	}

	public void setDoseLimit(String doseLimit) {
		this.doseLimit = doseLimit;
	}

	public String getDoseUnit() {
		return doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	public Float getResult() {
		return result;
	}

	public void setResult(Float result) {
		this.result = result;
	}

	public String getResultUnit() {
		return resultUnit;
	}

	public void setResultUnit(String resultUnit) {
		this.resultUnit = resultUnit;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getPlanLabel() {
		return planLabel;
	}

	public void setPlanLabel(String planLabel) {
		this.planLabel = planLabel;
	}

	public String getDoseSopUid() {
		return doseSopUid;
	}

	public void setDoseSopUid(String doseSopUid) {
		this.doseSopUid = doseSopUid;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getCalculationIdentifier() {
		return calculationIdentifier;
	}

	public void setCalculationIdentifier(String calculationIdentifier) {
		this.calculationIdentifier = calculationIdentifier;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DvhVolumeResultDto dvhVolumeResultDTO = (DvhVolumeResultDto) o;

        if ( ! Objects.equals(id, dvhVolumeResultDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

	@Override
	public String toString() {
		return "DvhVolumeResultDto [id=" + id + ", volumeOfInterest=" + volumeOfInterest + ", doseLimit=" + doseLimit
				+ ", doseUnit=" + doseUnit + ", result=" + result + ", resultUnit=" + resultUnit + ", error=" + error
				+ ", patientId=" + patientId + ", planLabel=" + planLabel + ", doseSopUid=" + doseSopUid
				+ ", containerId=" + containerId + ", calculationIdentifier=" + calculationIdentifier + ", version="
				+ version + "]";
	}
}
