package nl.maastro.mia.resultservice.web.controller.generated;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.resultservice.entity.DvhCurveResult;
import nl.maastro.mia.resultservice.service.generated.DvhCurveResultService;
import nl.maastro.mia.resultservice.web.controller.util.PaginationUtil;
import nl.maastro.mia.resultservice.web.dto.generated.DvhCurveResultDto;
import nl.maastro.mia.resultservice.web.mapper.DvhCurveResultMapper;

/**
 * REST controller for managing DvhCurveResult.
 */
@RestController
@RequestMapping("/api")
public class DvhCurveResultController {

    private final Logger log = LoggerFactory.getLogger(DvhCurveResultController.class);
        
    @Autowired
    private DvhCurveResultService dvhCurveResultService;
    
    @Autowired
    private DvhCurveResultMapper dvhCurveResultMapper;
    
    /**
     * GET  /dvh-curve-results : get all the dvhCurveResults.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dvhCurveResults in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/dvh-curve-results",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional(readOnly = true)
    public ResponseEntity<List<DvhCurveResultDto>> getAllDvhCurveResults(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of DvhCurveResults");
        Page<DvhCurveResult> page = dvhCurveResultService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dvh-curve-results");
        return new ResponseEntity<>(dvhCurveResultMapper.map(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /dvh-curve-results/:id : get the "id" dvhCurveResult.
     *
     * @param id the id of the dvhCurveResultDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dvhCurveResultDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/dvh-curve-results/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DvhCurveResultDto> getDvhCurveResult(@PathVariable Long id) {
        log.debug("REST request to get DvhCurveResult : {}", id);
        DvhCurveResultDto dvhCurveResultDTO = dvhCurveResultService.findOne(id);
        return Optional.ofNullable(dvhCurveResultDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
	/**
	 * SEARCH  /_search/containers?query=:query : search for the container corresponding
	 * to the query.
	 *
	 * @param query the query of the container search
	 * @return the result of the search
	 */
	@RequestMapping(value = "/_search/dvh-curve-results",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<DvhCurveResult>> search(@RequestParam String query, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to search for a page of Containers for query {} " + query);
		Page<DvhCurveResult> page = dvhCurveResultService.search(query, pageable);
		HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/dvh-curve-results");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

}
