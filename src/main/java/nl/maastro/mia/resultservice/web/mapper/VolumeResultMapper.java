package nl.maastro.mia.resultservice.web.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import nl.maastro.mia.resultservice.entity.VolumeResult;
import nl.maastro.mia.resultservice.web.dto.generated.VolumeResultDto;


@Service
public class VolumeResultMapper {

    public List<VolumeResultDto> map(List<VolumeResult> results){
    	
    	List<VolumeResultDto> list = new ArrayList<>();
    	for(VolumeResult result : results){
    		list.add(map(result));
    	}

		return list;    	
    }
    
    public VolumeResultDto map(VolumeResult result){
    	VolumeResultDto dto = new VolumeResultDto();
		
		dto.setId(result.getId());
		dto.setVersion(result.getVersion());
		dto.setCalculationIdentifier(result.getCalculationIdentifier());
		dto.setPatientId(result.getPatientId());
		dto.setPlanLabel(result.getPlanLabel());
		dto.setContainerId(result.getContainerId());
		dto.setVolumeOfInterest(result.getVolumeOfInterest());
		dto.setResult(result.getResult());
		dto.setStructSopUid(result.getStructSopUid());

		dto.setError(result.getError());
		
		return dto;
    }
}
