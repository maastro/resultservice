package nl.maastro.mia.resultservice.web.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import nl.maastro.mia.resultservice.entity.DoseResult;
import nl.maastro.mia.resultservice.web.dto.generated.DoseResultDto;


@Service
public class DoseResultMapper {

    public List<DoseResultDto> map(List<DoseResult> results){
    	
    	List<DoseResultDto> list = new ArrayList<>();
    	for(DoseResult result : results){
    		list.add(map(result));
    	}

		return list;    	
    }
    
    public DoseResultDto map(DoseResult result){
    	DoseResultDto dto = new DoseResultDto();
		
		dto.setId(result.getId());
		dto.setVersion(result.getVersion());
		dto.setCalculationIdentifier(result.getCalculationIdentifier());
		dto.setPatientId(result.getPatientId());
		dto.setPlanLabel(result.getPlanLabel());
		dto.setContainerId(result.getContainerId());
		dto.setVolumeOfInterest(result.getVolumeOfInterest());
		dto.setError(result.getError());
		dto.setDoseSopUid(result.getDoseSopUid());
		dto.setResult(result.getResult());

		dto.setOperation(result.getOperation());
		
		return dto;
    }
}
