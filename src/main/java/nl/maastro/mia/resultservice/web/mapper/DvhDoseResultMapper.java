package nl.maastro.mia.resultservice.web.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import nl.maastro.mia.resultservice.entity.DvhDoseResult;
import nl.maastro.mia.resultservice.web.dto.generated.DvhDoseResultDto;


@Service
public class DvhDoseResultMapper {

    public List<DvhDoseResultDto> map(List<DvhDoseResult> results){
    	
    	List<DvhDoseResultDto> list = new ArrayList<>();
    	for(DvhDoseResult result : results){
    		list.add(map(result));
    	}

		return list;    	
    }
    
    public DvhDoseResultDto map(DvhDoseResult result){
    	DvhDoseResultDto dto = new DvhDoseResultDto();
    	dto.setId(result.getId());
    	
		dto.setVolumeOfInterest(result.getVolumeOfInterest());
		dto.setVolumeLimit(result.getBoundary());
		dto.setVolumeUnit(result.getVolumeUnit());
		dto.setResult(result.getResult());
		dto.setResultUnit(result.getDoseUnit());
		dto.setTargetPrescriptionDose(result.getTargetPrescriptionDose());
		dto.setError(result.getError());
		
		dto.setPatientId(result.getPatientId());
		dto.setPlanLabel(result.getPlanLabel());
		dto.setDoseSopUid(result.getDoseSopUid());
		dto.setContainerId(result.getContainerId());
		dto.setCalculationIdentifier(result.getCalculationIdentifier());
		
		dto.setVersion(result.getVersion());
		return dto;
    }
}
