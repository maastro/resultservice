package nl.maastro.mia.resultservice.web.controller.generated;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.resultservice.entity.DvhDoseResult;
import nl.maastro.mia.resultservice.service.generated.DvhDoseResultService;
import nl.maastro.mia.resultservice.web.controller.util.PaginationUtil;
import nl.maastro.mia.resultservice.web.dto.generated.DvhDoseResultDto;
import nl.maastro.mia.resultservice.web.mapper.DvhDoseResultMapper;

/**
 * REST controller for managing DvhDoseResult.
 */
@RestController
@RequestMapping("/api")
public class DvhDoseResultController {

    private final Logger log = LoggerFactory.getLogger(DvhDoseResultController.class);
        
    @Autowired
    private DvhDoseResultService dvhDoseResultService;
    
    @Autowired
    private DvhDoseResultMapper dvhDoseResultMapper;

    /**
     * GET  /dvh-dose-results : get all the dvhDoseResults.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dvhDoseResults in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/dvh-dose-results",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional(readOnly = true)
    public ResponseEntity<List<DvhDoseResultDto>> getAllDvhDoseResults(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of DvhDoseResults");
        Page<DvhDoseResult> page = dvhDoseResultService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dvh-dose-results");
        return new ResponseEntity<>(dvhDoseResultMapper.map(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /dvh-dose-results/:id : get the "id" dvhDoseResult.
     *
     * @param id the id of the dvhDoseResultDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dvhDoseResultDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/dvh-dose-results/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DvhDoseResultDto> getDvhDoseResult(@PathVariable Long id) {
        log.debug("REST request to get DvhDoseResult : {}", id);
        DvhDoseResultDto dvhDoseResultDTO = dvhDoseResultService.findOne(id);
        return Optional.ofNullable(dvhDoseResultDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

	/**
	 * SEARCH  /_search/containers?query=:query : search for the container corresponding
	 * to the query.
	 *
	 * @param query the query of the container search
	 * @return the result of the search
	 */
	@RequestMapping(value = "/_search/dvh-dose-results",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<DvhDoseResult>> search(@RequestParam String query, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to search for a page of Containers for query {} " + query);
		Page<DvhDoseResult> page = dvhDoseResultService.search(query, pageable);
		HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/dvh-dose-results");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
}
