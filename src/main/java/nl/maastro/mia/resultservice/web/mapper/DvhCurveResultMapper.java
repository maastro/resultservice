package nl.maastro.mia.resultservice.web.mapper;



import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import nl.maastro.mia.resultservice.entity.DvhCurveResult;
import nl.maastro.mia.resultservice.web.dto.generated.DvhCurveResultDto;

@Service
public class DvhCurveResultMapper {


    public List<DvhCurveResultDto> map(List<DvhCurveResult> dvhCurveResults){
    	
    	List<DvhCurveResultDto> list = new ArrayList<>();
    	for(DvhCurveResult result : dvhCurveResults){
    		list.add(map(result));
    	}

		return list;    	
    }
    
    public DvhCurveResultDto map(DvhCurveResult result){
    	DvhCurveResultDto dto = new DvhCurveResultDto();
		
		dto.setId(result.getId());
		dto.setVersion(result.getVersion());
		dto.setCalculationIdentifier(result.getCalculationIdentifier());
		dto.setPatientId(result.getPatientId());
		dto.setPlanLabel(result.getPlanLabel());
		dto.setContainerId(result.getContainerId());
		dto.setVolumeOfInterest(result.getVolumeOfInterest());
		dto.setError(result.getError());
		dto.setDoseSopUid(result.getDoseSopUid());

		dto.setDoseSopUid(result.getDoseSopUid());
		dto.setDosevector(result.getDoseVector());
		dto.setVolumevector(result.getVolumeVector());
		dto.setVolumeUnit(result.getVolumeUnit());
		
		return dto;
    }
   

}
