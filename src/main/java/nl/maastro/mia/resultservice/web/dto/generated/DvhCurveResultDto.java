package nl.maastro.mia.resultservice.web.dto.generated;

import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the DvhCurveResult entity.
 */
public class DvhCurveResultDto implements Serializable {

    private Long id;

    private String version;


    private String calculationIdentifier;


    private String patientId;


    private String planLabel;


    private String containerId;


    private String volumeOfInterest;


    private String error;


    private String doseSopUid;


    private String volumeUnit;


    private String dosevector;


    private String volumevector;


    private Float binSize;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    public String getCalculationIdentifier() {
        return calculationIdentifier;
    }

    public void setCalculationIdentifier(String calculationIdentifier) {
        this.calculationIdentifier = calculationIdentifier;
    }
    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }
    public String getPlanLabel() {
        return planLabel;
    }

    public void setPlanLabel(String planLabel) {
        this.planLabel = planLabel;
    }
    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }
    public String getVolumeOfInterest() {
        return volumeOfInterest;
    }

    public void setVolumeOfInterest(String volumeOfInterest) {
        this.volumeOfInterest = volumeOfInterest;
    }
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
    public String getDoseSopUid() {
        return doseSopUid;
    }

    public void setDoseSopUid(String doseSopUid) {
        this.doseSopUid = doseSopUid;
    }
    public String getVolumeUnit() {
        return volumeUnit;
    }

    public void setVolumeUnit(String volumeUnit) {
        this.volumeUnit = volumeUnit;
    }
    public String getDosevector() {
        return dosevector;
    }

    public void setDosevector(String dosevector) {
        this.dosevector = dosevector;
    }
    public String getVolumevector() {
        return volumevector;
    }

    public void setVolumevector(String volumevector) {
        this.volumevector = volumevector;
    }
    public Float getBinSize() {
        return binSize;
    }

    public void setBinSize(Float binSize) {
        this.binSize = binSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DvhCurveResultDto dvhCurveResultDTO = (DvhCurveResultDto) o;

        if ( ! Objects.equals(id, dvhCurveResultDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DvhCurveResultDTO{" +
            "id=" + id +
            ", version='" + version + "'" +
            ", calculationIdentifier='" + calculationIdentifier + "'" +
            ", patientId='" + patientId + "'" +
            ", planLabel='" + planLabel + "'" +
            ", containerId='" + containerId + "'" +
            ", volumeOfInterest='" + volumeOfInterest + "'" +
            ", error='" + error + "'" +
            ", doseSopUid='" + doseSopUid + "'" +
            ", volumeUnit='" + volumeUnit + "'" +
            ", dosevector='" + dosevector + "'" +
            ", volumevector='" + volumevector + "'" +
            ", binSize='" + binSize + "'" +
            '}';
    }
}
