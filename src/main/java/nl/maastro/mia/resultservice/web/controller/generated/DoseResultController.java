package nl.maastro.mia.resultservice.web.controller.generated;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.resultservice.entity.DoseResult;
import nl.maastro.mia.resultservice.repository.DoseResultRepository;
import nl.maastro.mia.resultservice.service.generated.DoseResultService;
import nl.maastro.mia.resultservice.web.controller.util.PaginationUtil;

/**
 * REST controller for managing DoseResult.
 */
@RestController
@RequestMapping("/api")
public class DoseResultController {

    private final Logger log = LoggerFactory.getLogger(DoseResultController.class);
        
    @Autowired
    private DoseResultRepository doseResultRepository;
    
    @Autowired
    private DoseResultService doseResultService;

    /**
     * GET  /dose-results : get all the doseResults.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of doseResults in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/dose-results",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DoseResult>> getAllDoseResults(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of DoseResults");
        Page<DoseResult> page = doseResultRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dose-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /dose-results/:id : get the "id" doseResult.
     *
     * @param id the id of the doseResult to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the doseResult, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/dose-results/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DoseResult> getDoseResult(@PathVariable Long id) {
        log.debug("REST request to get DoseResult : {}", id);
        DoseResult doseResult = doseResultRepository.findOne(id);
        return Optional.ofNullable(doseResult)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    
	/**
	 * SEARCH  /_search/containers?query=:query : search for the container corresponding
	 * to the query.
	 *
	 * @param query the query of the container search
	 * @return the result of the search
	 */
	@RequestMapping(value = "/_search/dose-results",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<DoseResult>> search(@RequestParam String query, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to search for a page of Containers for query {} " + query);
		Page<DoseResult> page = doseResultService.search(query, pageable);
		HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/dose-results");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

}
