package nl.maastro.mia.resultservice.web.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import nl.maastro.mia.resultservice.entity.DvhVolumeResult;
import nl.maastro.mia.resultservice.web.dto.generated.DvhVolumeResultDto;

@Service
public class DvhVolumeResultMapper {

    public List<DvhVolumeResultDto> map(List<DvhVolumeResult> results){
    	
    	List<DvhVolumeResultDto> list = new ArrayList<>();
    	for(DvhVolumeResult result : results){
    		list.add(map(result));
    	}

		return list;    	
    }
    
    public DvhVolumeResultDto map(DvhVolumeResult result){
    	DvhVolumeResultDto dto = new DvhVolumeResultDto();
    	dto.setId(result.getId());
    	
		dto.setVolumeOfInterest(result.getVolumeOfInterest());
		dto.setDoseLimit(result.getBoundary());
		dto.setDoseUnit(result.getDoseUnit());
		dto.setResult(result.getResult());
		dto.setResultUnit(result.getVolumeUnit());
		dto.setError(result.getError());
		
		dto.setPatientId(result.getPatientId());
		dto.setPlanLabel(result.getPlanLabel());
		dto.setDoseSopUid(result.getDoseSopUid());
		dto.setContainerId(result.getContainerId());
		dto.setCalculationIdentifier(result.getCalculationIdentifier());
		
		dto.setVersion(result.getVersion());
		return dto;
    }
}
