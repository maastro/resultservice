package nl.maastro.mia.resultservice.web.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.resultservice.config.RdfConfiguration;
import nl.maastro.mia.resultservice.service.SparqlInsertService;
import nl.maastro.mia.resultservice.service.SqlInsertService;
import nl.maastro.mia.resultservice.web.dto.ComputationResultsDto;

@RestController
@RequestMapping("/api")
@EnableConfigurationProperties(RdfConfiguration.class)
public class StoreController {
	private static final Logger LOGGER = Logger.getLogger(StoreController.class);
	
	@Autowired
	private SqlInsertService sqlInsertService;
	
	@Autowired
	private SparqlInsertService sparqlInsertService;
	
	@Autowired
	private RdfConfiguration rdfConfiguration;

	
	@RequestMapping(value="/store", method=RequestMethod.POST)
	public ResponseEntity<Boolean> storeCalculationResult(@RequestBody ComputationResultsDto computationResultsDto,
			BindingResult result){
		
		if (computationResultsDto == null){
			LOGGER.warn("computationResultDto is null.");
	        return new ResponseEntity<>(new Boolean(false), HttpStatus.BAD_REQUEST);
		}
		boolean inputValid = this.checkInputvalidity(computationResultsDto);
		if(!inputValid){
			return new ResponseEntity<>(new Boolean(false), HttpStatus.BAD_REQUEST);
		}
		
		if (rdfConfiguration.isEnabled()){
			return new ResponseEntity<>(sparqlInsertService.insertResults(computationResultsDto), HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(sqlInsertService.insertResults(computationResultsDto), HttpStatus.OK);
		}
	}

	private boolean checkInputvalidity(ComputationResultsDto computationResultsDto) {
		if (computationResultsDto.getSopUids() == null || computationResultsDto.getSopUids().isEmpty()){
			LOGGER.warn("SopUid map is null or empty.");
			return false;
		}
		if (computationResultsDto.getPatientId()==null || computationResultsDto.getPatientId().isEmpty()){
			LOGGER.warn("PatientId is null or empty.");
			return false;
		}
		if (computationResultsDto.getComputationResults() == null){
			LOGGER.warn("Computation results is null");
			return false;
		}
		if (computationResultsDto.getComputationResults().isEmpty()){
			LOGGER.warn("No results to export.");
		}
		
		return true;
	}
	
}