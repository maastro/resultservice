package nl.maastro.mia.resultservice.web.dto.generated;

import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the VolumeResult entity.
 */
public class VolumeResultDto implements Serializable {

    private Long id;

    private String structSopUid;


    private Float result;


    private String version;


    private String calculationIdentifier;


    private String patientId;


    private String planLabel;


    private String containerId;


    private String volumeOfInterest;


    private String error;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getStructSopUid() {
		return structSopUid;
	}

	public void setStructSopUid(String structSopUid) {
		this.structSopUid = structSopUid;
	}

	public Float getResult() {
        return result;
    }

    public void setResult(Float result) {
        this.result = result;
    }
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    public String getCalculationIdentifier() {
        return calculationIdentifier;
    }

    public void setCalculationIdentifier(String calculationIdentifier) {
        this.calculationIdentifier = calculationIdentifier;
    }
    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }
    public String getPlanLabel() {
        return planLabel;
    }

    public void setPlanLabel(String planLabel) {
        this.planLabel = planLabel;
    }
    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }
    public String getVolumeOfInterest() {
        return volumeOfInterest;
    }

    public void setVolumeOfInterest(String volumeOfInterest) {
        this.volumeOfInterest = volumeOfInterest;
    }
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VolumeResultDto volumeResultDTO = (VolumeResultDto) o;

        if ( ! Objects.equals(id, volumeResultDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "VolumeResultDTO{" +
            "id=" + id +
            ", structSopUid='" + structSopUid + "'" +
            ", result='" + result + "'" +
            ", version='" + version + "'" +
            ", calculationIdentifier='" + calculationIdentifier + "'" +
            ", patientId='" + patientId + "'" +
            ", planLabel='" + planLabel + "'" +
            ", containerId='" + containerId + "'" +
            ", volumeOfInterest='" + volumeOfInterest + "'" +
            ", error='" + error + "'" +
            '}';
    }
}
