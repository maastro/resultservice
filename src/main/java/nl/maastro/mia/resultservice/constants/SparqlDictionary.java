package nl.maastro.mia.resultservice.constants;

public class SparqlDictionary {
	public static final String RADIATION_ONCOLOGY_REGION_OF_INTEREST = "100067";
	public static final String XSD_STRING_LOCAL = "@local";
	public static final String HAS_OPERATOR = "hasOperator";
	public static final String HAS_ROI = "hasRoi";
	public static final String DATATYPE = "Datatype";
	public static final String LABEL = "label";
}