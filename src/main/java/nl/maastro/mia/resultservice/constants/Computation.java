package nl.maastro.mia.resultservice.constants;

public class Computation {
	
	public static final String RTDOSE = "RTDOSE";
	public static final String RTSTRUCT = "RTSTRUCT";
	
	public static final String DOSE_COMPUTATION = "DoseComputation";
	public static final String DVH_CURVE_COMPUTATION = "DvhCurveComputation";
	public static final String DVH_DOSE_COMPUTATION = "DvhDoseComputation";
	public static final String DVH_VOLUME_COMPUTATION = "DvhVolumeComputation";
	public static final String VOLUME_COMPUTATION = "VolumeComputation";
	
	public static final String DOSE_OPERATION = "doseOperation";
	public static final String RESULT = "result";
	public static final String IS_ABSOLUTE = "isAbsolute";
	public static final String BINSIZE = "binsize";
	public static final String VOLUME_VECTOR = "volumevector";
	public static final String DOSE_VECTOR = "dosevector";
	public static final String TARGET_PRESCRIPTION_DOSE = "targetPrescriptionDose";
	public static final String LIMIT = "limit";
	public static final String LIMIT_UNIT = "limitUnit";
	public static final String ABSOLUTE_OUTPUT = "absoluteOutput";
	
	
	private Computation(){}
}
