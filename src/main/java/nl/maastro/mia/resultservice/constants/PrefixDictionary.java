package nl.maastro.mia.resultservice.constants;

public enum PrefixDictionary {
	RDF("rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#"),
	RDFS("rdfs","http://www.w3.org/2000/01/rdf-schema#"),
	SEDI("sedi","http://semantic-dicom.org/dcm#"),
	MIA("mia","http://cancerdata.org/mia/miaData#"),
	UO("uo","http://http://purl.obolibrary.org/obo/UO_"),
	XSD("xsd","http://www.w3.org/2001/XMLSchema#"),
	NCIT("ncit","http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#"),
	ROO("roo","http://www.cancerdata.org/roo#"),
	OWL("owl","http://www.w3.org/2002/07/owl#");
	
	String name;
	String location;
	
	private PrefixDictionary(String name, String location){
		this.name = name;
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public String getLocation() {
		return location;
	}
}