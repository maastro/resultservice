package nl.maastro.mia.resultservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="rdf")
public class RdfConfiguration {
	
	private String endpoint = "http://localhost:9999/blazegraph/namespace/kb/sparql";
	
	private boolean enabled = false;
	
	private String queryfolder = "./sparqlqueries";

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getQueryfolder() {
		return queryfolder;
	}

	public void setQueryfolder(String queryfolder) {
		this.queryfolder = queryfolder;
	}

	
}
