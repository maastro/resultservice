package nl.maastro.mia.resultservice.service;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.maastro.mia.resultservice.data.sparql.SparqlInsert;
import nl.maastro.mia.resultservice.repository.sparql.SparqlResultRepository;
import nl.maastro.mia.resultservice.service.rdf.SparqlTtlService;
import nl.maastro.mia.resultservice.web.dto.ComputationResultDto;
import nl.maastro.mia.resultservice.web.dto.ComputationResultsDto;
import nl.maastro.mia.resultservice.web.dto.VolumeOfInterest;

@Service
public class SparqlInsertService {
	private static final Logger LOGGER = Logger.getLogger(SparqlInsertService.class);
	
	@Autowired
	private SparqlTtlService sparqlTtlService;
	
	@Autowired
	private SparqlResultRepository sparqlResultRepository;

	public boolean insertResults(ComputationResultsDto computationResultsDto) {
		
		
		String query = null;
		SparqlInsert volumeOfInterestQuery = null;
		boolean volumeOfInterestInserted = true;
		boolean resultsInserted = true;
		try {
			for(ComputationResultDto result : computationResultsDto.getComputationResults()){
				
				LOGGER.info("Storing computation result into SPARQL:" + result.getModuleName());
				
				if (result.getVolumesOfInterest() != null) {
					for (VolumeOfInterest voi : result.getVolumesOfInterest()) {
						if (voi.getOperators() != null && voi.getRois() != null) {
							volumeOfInterestQuery = sparqlTtlService.createVolumeOfInterestQuery(voi);
							LOGGER.trace("volume of interest query: " + volumeOfInterestQuery);
							LOGGER.info("SPARQL update for voi: " + voi.getName()  + " taskid: " + computationResultsDto.getTaskId());
							boolean computationVolumeOfInterestInserted = sparqlResultRepository.updateQuery(volumeOfInterestQuery.toString());
							if(!computationVolumeOfInterestInserted){
								volumeOfInterestInserted = false;
							}
						}
					}
				}
				
				
				query = sparqlTtlService.getQueryForComputation(result, computationResultsDto);
				LOGGER.info("SPARQL update for result: " + result.getModuleName() + " taskid: " + computationResultsDto.getTaskId());
				boolean computationResultsInserted = sparqlResultRepository.updateQuery(query);
				if(!computationResultsInserted){
					resultsInserted = false;
				}
				
			}
		} catch (IOException e) {
			LOGGER.error("IO Exception reading sparql query from file.",e);
			return false;
		}


		return resultsInserted && volumeOfInterestInserted;
	}


}
