package nl.maastro.mia.resultservice.service.generated;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import nl.maastro.mia.resultservice.entity.DoseResult;
import nl.maastro.mia.resultservice.repository.DoseResultRepository;
import nl.maastro.mia.resultservice.web.dto.generated.DoseResultDto;
import nl.maastro.mia.resultservice.web.mapper.DoseResultMapper;

/**
 * Service Implementation for managing DoseResult.
 */
@Service
@Transactional
public class DoseResultService {

    private final Logger log = LoggerFactory.getLogger(DoseResultService.class);
    
    @Autowired
    private DoseResultRepository doseResultRepository;
    
    @Autowired
    private DoseResultMapper doseResultMapper;

    /**
     *  Get all the doseResults.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<DoseResult> findAll(Pageable pageable) {
        log.debug("Request to get all DoseResults");
        Page<DoseResult> result = doseResultRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one doseResult by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public DoseResultDto findOne(Long id) {
        log.debug("Request to get DoseResult : {}", id);
        DoseResult doseResult = doseResultRepository.findOne(id);
        DoseResultDto doseResultDTO = doseResultMapper.map(doseResult);
        return doseResultDTO;
    }

	public Page<DoseResult> search(String query, Pageable pageable) {
		return doseResultRepository.findByPatientId(query, pageable);
	}


}
