package nl.maastro.mia.resultservice.service.rdf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.maastro.mia.resultservice.constants.SparqlDictionary;
import nl.maastro.mia.resultservice.data.sparql.Triple;
import nl.maastro.mia.resultservice.data.sparql.TripleElement;
import nl.maastro.mia.resultservice.web.dto.VolumeOfInterest;

@Service
public class TripleCreateService {
	@Autowired
	ElementCreateService elementCreateService;

	public Triple createRegionOfInterestLabelTriple(String roi) {
		TripleElement regionOfInterestIdentifier = 
				elementCreateService.getRegionOfInterestIdentifier(roi); 
		TripleElement labelPredicate = elementCreateService.getLabelPredicate();

		String regionOfInterestLabel = "\"" + roi + "\""+ SparqlDictionary.XSD_STRING_LOCAL;
		return new Triple(regionOfInterestIdentifier.toString(),
				labelPredicate.toString(),
				regionOfInterestLabel);
	}

	public Triple createRegionOfInterestTriple(VolumeOfInterest volumeOfInterest,
			String roi) {
		TripleElement volumeOfInterestIdentifier =
				elementCreateService.getVolumeOfInterestIdentifier(volumeOfInterest);
		TripleElement hasRoiPredicate = elementCreateService.getRoiPredicate();
		TripleElement regionOfInterest = elementCreateService.getRegionOfInterest(roi);
		return new Triple(volumeOfInterestIdentifier.toString(),
				hasRoiPredicate.toString(),
				regionOfInterest.toString());
	}

	public Triple createVolumeOfInterestTriple(VolumeOfInterest volumeOfInterest) {
		TripleElement volumeOfInterestIdentifier = 
				elementCreateService.getVolumeOfInterestIdentifier(volumeOfInterest);
		TripleElement labelPredicate = elementCreateService.getLabelPredicate();
		String volumeOfInterestLabel = "\"" 
				+ volumeOfInterest.getName() 
				+ "\"" 
				+ SparqlDictionary.XSD_STRING_LOCAL;
		return new Triple(volumeOfInterestIdentifier.toString(),
				labelPredicate.toString(),
				volumeOfInterestLabel);
	}

	public Triple createRegionOfInterestTypeTriple(String roi) {
		TripleElement regionOfInterestIdentifier = 
				elementCreateService.getRegionOfInterestIdentifier(roi); 
		TripleElement dataType = 
				elementCreateService.getDataTypePredicate(); 
		TripleElement radiationTypeOncologyRegionOfInterest = 
				elementCreateService.getRadiationOncologyRegionOfInterest(); 
		return new Triple(regionOfInterestIdentifier.toString(),
				dataType.toString(),
				radiationTypeOncologyRegionOfInterest.toString());
	}

	public Triple createOperatorTriple(String roi, String operator) {
		TripleElement regionOfInterestIdentifier = 
				elementCreateService.getRegionOfInterestIdentifier(roi); 
		TripleElement dataType = 
				elementCreateService.getHasOperatorPredicate(); 
		String operatorLabel = "\"" + operator + "\""+ SparqlDictionary.XSD_STRING_LOCAL;
		return new Triple(regionOfInterestIdentifier.toString(),
				dataType.toString(),
				operatorLabel);
	}
}