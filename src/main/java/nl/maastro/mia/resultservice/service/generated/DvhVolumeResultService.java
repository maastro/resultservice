package nl.maastro.mia.resultservice.service.generated;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import nl.maastro.mia.resultservice.entity.DvhVolumeResult;
import nl.maastro.mia.resultservice.repository.DvhVolumeResultRepository;
import nl.maastro.mia.resultservice.web.dto.generated.DvhVolumeResultDto;
import nl.maastro.mia.resultservice.web.mapper.DvhVolumeResultMapper;

/**
 * Service Implementation for managing DvhVolumeResult.
 */
@Service
@Transactional
public class DvhVolumeResultService {

    private final Logger log = LoggerFactory.getLogger(DvhVolumeResultService.class);
    
    @Autowired
    private DvhVolumeResultRepository dvhVolumeResultRepository;
    
    @Autowired
    private DvhVolumeResultMapper dvhVolumeResultMapper;
    
    /**
     *  Get all the dvhVolumeResults.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<DvhVolumeResult> findAll(Pageable pageable) {
        log.debug("Request to get all DvhVolumeResults");
        Page<DvhVolumeResult> result = dvhVolumeResultRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one dvhVolumeResult by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public DvhVolumeResultDto findOne(Long id) {
        log.debug("Request to get DvhVolumeResult : {}", id);
        DvhVolumeResult dvhVolumeResult = dvhVolumeResultRepository.findOne(id);
        DvhVolumeResultDto dvhVolumeResultDTO = dvhVolumeResultMapper.map(dvhVolumeResult);
        return dvhVolumeResultDTO;
    }
    
	public Page<DvhVolumeResult> search(String query, Pageable pageable) {
		return dvhVolumeResultRepository.findByPatientId(query, pageable);
	}
}
