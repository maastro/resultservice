package nl.maastro.mia.resultservice.service.rdf;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.maastro.mia.resultservice.constants.PrefixDictionary;
import nl.maastro.mia.resultservice.data.sparql.Prefix;
import nl.maastro.mia.resultservice.data.sparql.PrefixBlock;
import nl.maastro.mia.resultservice.data.sparql.Triple;
import nl.maastro.mia.resultservice.web.dto.VolumeOfInterest;

@Service
public class InsertQueryBuilderService {
	@Autowired
	TripleCreateService tripleCreateService;

	public PrefixBlock getDefaultPrefixes(){
		PrefixBlock prefixBlock = new PrefixBlock();
		List<Prefix> prefixes = new ArrayList<>();
		for (PrefixDictionary prefixDict : PrefixDictionary.values()){
			Prefix prefix = new Prefix(prefixDict.getName(), prefixDict.getLocation());
			prefixes.add(prefix);
		}
		prefixBlock.setPrefixes(prefixes);
		return prefixBlock;
	}

	public List<Triple> createRegionOfInterestTriples(VolumeOfInterest volumeOfInterest) {
		List<String> regionsOfInterest = volumeOfInterest.getRois();
		List<String> operators = volumeOfInterest.getOperators();

		volumeOfInterest.getOperators().add(0, "+");
		Iterator<String> operatorIterator = operators.iterator();
		Iterator<String> regionOfInterestIterator = regionsOfInterest.iterator();
		List<Triple> triples = new ArrayList<>();
		while (operatorIterator.hasNext() && regionOfInterestIterator.hasNext()) {
			String roi = regionOfInterestIterator.next();
			String operator = operatorIterator.next();
			triples.add(tripleCreateService.createRegionOfInterestTriple(volumeOfInterest, roi));
			triples.add(tripleCreateService.createRegionOfInterestLabelTriple(roi));
			triples.add(tripleCreateService.createRegionOfInterestTypeTriple(roi));
			triples.add(tripleCreateService.createOperatorTriple(roi, operator));
		}

		return triples;
	}
}