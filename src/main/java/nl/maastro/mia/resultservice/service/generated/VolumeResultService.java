package nl.maastro.mia.resultservice.service.generated;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import nl.maastro.mia.resultservice.entity.VolumeResult;
import nl.maastro.mia.resultservice.repository.VolumeResultRepository;
import nl.maastro.mia.resultservice.web.dto.generated.VolumeResultDto;
import nl.maastro.mia.resultservice.web.mapper.VolumeResultMapper;

/**
 * Service Implementation for managing VolumeResult.
 */
@Service
@Transactional
public class VolumeResultService {

    private final Logger log = LoggerFactory.getLogger(VolumeResultService.class);
    
    @Autowired
    private VolumeResultRepository volumeResultRepository;
    
    @Autowired
    private VolumeResultMapper volumeResultMapper;

    /**
     *  Get all the volumeResults.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<VolumeResult> findAll(Pageable pageable) {
        log.debug("Request to get all VolumeResults");
        Page<VolumeResult> result = volumeResultRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one volumeResult by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public VolumeResultDto findOne(Long id) {
        log.debug("Request to get VolumeResult : {}", id);
        VolumeResult volumeResult = volumeResultRepository.findOne(id);
        VolumeResultDto volumeResultDTO = volumeResultMapper.map(volumeResult);
        return volumeResultDTO;
    }
    
	public Page<VolumeResult> search(String query, Pageable pageable) {
		return volumeResultRepository.findByPatientId(query, pageable);
	}

}
