package nl.maastro.mia.resultservice.service.rdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import nl.maastro.mia.resultservice.config.RdfConfiguration;
import nl.maastro.mia.resultservice.data.sparql.InsertClause;
import nl.maastro.mia.resultservice.data.sparql.SparqlInsert;
import nl.maastro.mia.resultservice.data.sparql.Triple;
import nl.maastro.mia.resultservice.data.sparql.WhereClause;
import nl.maastro.mia.resultservice.web.dto.ComputationResultDto;
import nl.maastro.mia.resultservice.web.dto.ComputationResultsDto;
import nl.maastro.mia.resultservice.web.dto.VolumeOfInterest;

@Service
@EnableConfigurationProperties(RdfConfiguration.class)
public class SparqlTtlService implements InitializingBean{


	private static final Logger LOGGER = Logger.getLogger(SparqlTtlService.class);

	private static final String INSERTION_IDENTIFIER = "##";
	private static final String VOLUMEOFINTEREST_IDENTIFIER = "##VOLUMEOFINTEREST##";
	private static final String PATIENT_IDENTIFIER = "##PATIENTID##";
	private static final String VERSION_IDENTIFIER = "##VERSION##";
	private static final String ERROR_IDENTIFIER = "##ERROR##";
	
	private static final String UUID_IDENTIFIER = "##UUID##";
	private static final String TASK_IDENTIFIER = "##TASKID##";


	@Autowired
	private TripleCreateService tripleCreateService;

	@Autowired
	private InsertQueryBuilderService insertQueryBuilderService;

	@Autowired
	private RdfConfiguration rdfConfiguration;


	private Map<String, String> sparqlQueries = new HashMap<String, String>();

	@Override
	public void afterPropertiesSet() throws Exception {
		File queryFolder = new File(rdfConfiguration.getQueryfolder());
		LOGGER.debug("Query folder location: " + queryFolder.getAbsolutePath());
		if(!queryFolder.isDirectory() || queryFolder.listFiles() == null){
			LOGGER.error("Query folder doesn't point to a valid folder.");
			return;
		}
		for(File queryFile : queryFolder.listFiles()){
			String queryTemplateValue = new String(Files.readAllBytes(queryFile.toPath()));
			String fileName = queryFile.getName();
			//regex to get filename without extension
			String fileNameKey = fileName.replaceFirst("[.][^.]+$", "").toLowerCase();
			sparqlQueries.put(fileNameKey, queryTemplateValue);
			LOGGER.debug(System.lineSeparator() + "SPARQL QUERY loaded: " + fileNameKey + System.lineSeparator() + queryTemplateValue);
		}
	}

	public String getQueryForComputation(ComputationResultDto computationResult, ComputationResultsDto computationResultsDto) 
			throws IOException {

		String key = computationResult.getModuleName().toLowerCase();
		if(!sparqlQueries.containsKey(key)){
			throw new FileNotFoundException("No sparqlQueries for " + computationResult.getModuleName().toLowerCase());
		}
		String filledQuery = fillQuery(computationResult, computationResultsDto, sparqlQueries.get(key));
		
		return filledQuery;
	}

	private String fillQuery(ComputationResultDto computationResultDto, ComputationResultsDto computationResultsDto, String queryTemplate){

		String queryFilled = insertResults(computationResultDto.getResults(), queryTemplate);
		LOGGER.trace("query after inserting results: " + queryFilled);

		String uuid = UUID.randomUUID().toString();
		queryFilled = queryFilled.replaceAll(UUID_IDENTIFIER, uuid);
		LOGGER.trace("query after inserting UUID: " + queryFilled);

		queryFilled = queryFilled.replaceAll(TASK_IDENTIFIER, computationResultsDto.getTaskId());
		LOGGER.trace("query after inserting TASKID: " + queryFilled);
		
		if(computationResultsDto.getPatientId() != null){
			queryFilled = queryFilled.replaceAll(PATIENT_IDENTIFIER,computationResultsDto.getPatientId());
		}
		LOGGER.trace("query after inserting patientid: " + queryFilled);

		if(computationResultDto.getVersion() != null){
			queryFilled = queryFilled.replaceAll(VERSION_IDENTIFIER, computationResultDto.getVersion());
		}
		LOGGER.trace("query after inserting version: " + queryFilled);

		if(computationResultDto.getVolumesOfInterest() != null){
			queryFilled = queryFilled.replaceAll(VOLUMEOFINTEREST_IDENTIFIER, 
					computationResultDto.getVolumesOfInterest().get(0).getName());
		}
		LOGGER.trace("query after inserting volume of interest: " + queryFilled);

		if(computationResultsDto.getSopUids() != null){
			queryFilled = insertSopUids(computationResultsDto.getSopUids(), queryFilled);
		}
		LOGGER.trace("query after inserting sop uids: " + queryFilled);
		
		if(computationResultDto.getError()==null)
			queryFilled = queryFilled.replaceAll(ERROR_IDENTIFIER,"");
		else{
			LOGGER.warn("Result contained error: " + computationResultDto.getError());
			queryFilled = queryFilled.replaceAll(ERROR_IDENTIFIER, computationResultDto.getError());
		}
		
		queryFilled = queryFilled.replaceAll(INSERTION_IDENTIFIER +	"[a-zA-Z0-9]*" + INSERTION_IDENTIFIER,"null");
		LOGGER.trace("query after inserting nulls: " + queryFilled);
		
		if(computationResultDto.getError()!=null)
			queryFilled = removeNullLines(queryFilled);
			
		return queryFilled;
	}


	private String removeNullLines(String input){
		
		String[] lines = input.split(System.lineSeparator());
		StringBuilder finalStringBuilder = new StringBuilder();
		for(String s:lines){
		   if(!s.contains("null")){
		       finalStringBuilder.append(s).append(System.lineSeparator());
		    }
		}
		return finalStringBuilder.toString();
	}

	private String insertResults(Map<String, String> map, String query) {
		String filledQuery = query;
		for(Entry<String, String> result : map.entrySet()){
			filledQuery = filledQuery.replaceAll(INSERTION_IDENTIFIER +
					result.getKey() +
					INSERTION_IDENTIFIER, result.getValue());
		}
		return filledQuery;
	}

	private String insertSopUids(Map<String, List<String>> map, String query) {
		String filledQuery = query;
		for(Entry<String, List<String>> sopUids : map.entrySet()){
			String sopUid = null;
			if(!sopUids.getValue().isEmpty()){
				sopUid = sopUids.getValue().get(0);
			}
			filledQuery = filledQuery.replaceAll(INSERTION_IDENTIFIER +
					sopUids.getKey() +
					INSERTION_IDENTIFIER,sopUid);
		}
		return filledQuery;
	}

	public SparqlInsert createVolumeOfInterestQuery(VolumeOfInterest volumeOfInterest) {
		Triple volumeOfInterestTriple = tripleCreateService.
				createVolumeOfInterestTriple(volumeOfInterest);
		List<Triple> regionOfInterestTriples = insertQueryBuilderService
				.createRegionOfInterestTriples(volumeOfInterest);
		regionOfInterestTriples.add(volumeOfInterestTriple);

		SparqlInsert volumeOfInterestQuery = new SparqlInsert();
		volumeOfInterestQuery.setPrefixBlock(insertQueryBuilderService.getDefaultPrefixes());
		volumeOfInterestQuery.setInsertClause(new InsertClause(regionOfInterestTriples));
		volumeOfInterestQuery.setWhereClause(new WhereClause(Collections.emptyList()));

		return volumeOfInterestQuery;
	}

}