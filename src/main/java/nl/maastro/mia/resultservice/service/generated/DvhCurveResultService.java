package nl.maastro.mia.resultservice.service.generated;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import nl.maastro.mia.resultservice.entity.DvhCurveResult;
import nl.maastro.mia.resultservice.repository.DvhCurveResultRepository;
import nl.maastro.mia.resultservice.web.dto.generated.DvhCurveResultDto;
import nl.maastro.mia.resultservice.web.mapper.DvhCurveResultMapper;

/**
 * Service Implementation for managing DvhCurveResult.
 */
@Service
@Transactional
public class DvhCurveResultService {

    private final Logger log = LoggerFactory.getLogger(DvhCurveResultService.class);
    
    @Autowired
    private DvhCurveResultRepository dvhCurveResultRepository;
    
    @Autowired
    private DvhCurveResultMapper dvhCurveResultMapper;

    /**
     *  Get all the dvhCurveResults.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<DvhCurveResult> findAll(Pageable pageable) {
        log.debug("Request to get all DvhCurveResults");
        Page<DvhCurveResult> result = dvhCurveResultRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one dvhCurveResult by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public DvhCurveResultDto findOne(Long id) {
        log.debug("Request to get DvhCurveResult : {}", id);
        DvhCurveResult dvhCurveResult = dvhCurveResultRepository.findOne(id);
        DvhCurveResultDto dvhCurveResultDTO = dvhCurveResultMapper.map(dvhCurveResult);
        return dvhCurveResultDTO;
    }
    
	public Page<DvhCurveResult> search(String query, Pageable pageable) {
		return dvhCurveResultRepository.findByPatientId(query, pageable);
	}


}
