package nl.maastro.mia.resultservice.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.maastro.mia.resultservice.constants.Computation;
import nl.maastro.mia.resultservice.entity.DoseResult;
import nl.maastro.mia.resultservice.entity.DvhCurveResult;
import nl.maastro.mia.resultservice.entity.DvhDoseResult;
import nl.maastro.mia.resultservice.entity.DvhVolumeResult;
import nl.maastro.mia.resultservice.entity.GenericResult;
import nl.maastro.mia.resultservice.entity.VolumeResult;
import nl.maastro.mia.resultservice.repository.DoseResultRepository;
import nl.maastro.mia.resultservice.repository.DvhCurveResultRepository;
import nl.maastro.mia.resultservice.repository.DvhDoseResultRepository;
import nl.maastro.mia.resultservice.repository.DvhVolumeResultRepository;
import nl.maastro.mia.resultservice.repository.VolumeResultRepository;
import nl.maastro.mia.resultservice.web.dto.ComputationResultDto;
import nl.maastro.mia.resultservice.web.dto.ComputationResultsDto;
import nl.maastro.mia.resultservice.web.dto.VolumeOfInterest;

@Service
public class SqlInsertService {

	private static final Logger LOGGER = Logger.getLogger(SqlInsertService.class);

	@Autowired
	private DoseResultRepository doseResultReposity;

	@Autowired
	private DvhCurveResultRepository dvhCurveResultRepository;

	@Autowired
	private DvhDoseResultRepository dvhDoseResultRepository;

	@Autowired
	private DvhVolumeResultRepository dvhVolumeResultRepository;

	@Autowired
	private VolumeResultRepository volumeResultRepository;


	public boolean insertResults(ComputationResultsDto computationResultsDto) {
		boolean anyInsertFailed = false;

		for (ComputationResultDto computationResult : computationResultsDto.getComputationResults()){
			LOGGER.info("Storing computation result into SQL: " + computationResult.getModuleName());

			if (!this.saveRelevantResult(computationResult, computationResultsDto)){
				anyInsertFailed = true;
			}
		}

		return !anyInsertFailed;
	}

	private boolean saveRelevantResult(ComputationResultDto computationResult, ComputationResultsDto computationResultsDto){
		if (computationResult == null){
			LOGGER.error("ComputationResult is null. Cannot proceed.");
			return false;
		}

		switch (computationResult.getModuleName()){

		case Computation.DOSE_COMPUTATION:
			return this.saveDoseComputation(computationResult, computationResultsDto);

		case Computation.DVH_CURVE_COMPUTATION:
			return this.saveDvhCurveComputation(computationResult, computationResultsDto);

		case Computation.DVH_DOSE_COMPUTATION:
			return this.saveDvhDoseComputation(computationResult, computationResultsDto);

		case Computation.DVH_VOLUME_COMPUTATION:
			return this.saveDvhVolumeComputation(computationResult, computationResultsDto);

		case Computation.VOLUME_COMPUTATION:
			return this.saveVolumeComputation(computationResult, computationResultsDto);

		default:
			LOGGER.warn(computationResult.getModuleName() + " is not a valid computation identifier name. Cannot proceed.");
			return false;
		}
	}

	private boolean saveDoseComputation(ComputationResultDto result, ComputationResultsDto computationResultsDto){
		DoseResult doseResult = this.setGetBaseResultAttributes(result, computationResultsDto, new DoseResult());

		doseResult.setDoseSopUid(computationResultsDto.getSopUids().get(Computation.RTDOSE).get(0));
		doseResult.setOperation(result.getResults().get(Computation.DOSE_OPERATION));

		try{
			String resultFloat = result.getResults().get(Computation.RESULT);
			if(resultFloat != null){
				doseResult.setResult(Float.parseFloat(resultFloat));
			}
		}
		catch(NumberFormatException e){
			LOGGER.warn("doseResult is not a Float patientId:" + computationResultsDto.getPatientId());
		}

		return doseResultReposity.save(doseResult) != null;
	}

	private boolean saveDvhCurveComputation(ComputationResultDto result, ComputationResultsDto computationResultsDto){
		DvhCurveResult dvhCurveResult = this.setGetBaseResultAttributes(result, computationResultsDto, new DvhCurveResult());

		dvhCurveResult.setDoseSopUid(computationResultsDto.getSopUids().get(Computation.RTDOSE).get(0));

		dvhCurveResult.setVolumeUnit(Boolean.valueOf(result.getResults().get(Computation.IS_ABSOLUTE)) ? "cc" : "%");

		dvhCurveResult.setVolumeVector(result.getResults().get(Computation.VOLUME_VECTOR));
		dvhCurveResult.setDoseVector(result.getResults().get(Computation.DOSE_VECTOR));

		return dvhCurveResultRepository.save(dvhCurveResult) != null;
	}

	private boolean saveDvhDoseComputation(ComputationResultDto result, ComputationResultsDto computationResultsDto){
		DvhDoseResult dvhDoseResult = this.setGetBaseResultAttributes(result, computationResultsDto, new DvhDoseResult());

		dvhDoseResult.setDoseSopUid(computationResultsDto.getSopUids().get(Computation.RTDOSE).get(0));

		dvhDoseResult.setDoseUnit(Boolean.valueOf(result.getResults().get(Computation.ABSOLUTE_OUTPUT)) ? "cc" : "%");
		dvhDoseResult.setBoundary(result.getResults().get(Computation.LIMIT));
		dvhDoseResult.setVolumeUnit(result.getResults().get(Computation.LIMIT_UNIT));
		dvhDoseResult.setTargetPrescriptionDose(result.getResults().get(Computation.TARGET_PRESCRIPTION_DOSE));

		try{
			String resultFloat = result.getResults().get(Computation.RESULT);
			if(resultFloat != null){
				dvhDoseResult.setResult(Float.parseFloat(resultFloat));
			}
		}
		catch(NumberFormatException e){
			LOGGER.warn("dvhDoseResult result is not a Float patientId:" + computationResultsDto.getPatientId());
		}

		return dvhDoseResultRepository.save(dvhDoseResult) != null;
	}

	private boolean saveDvhVolumeComputation(ComputationResultDto result, ComputationResultsDto computationResultsDto){
		DvhVolumeResult dvhVolumeResult = this.setGetBaseResultAttributes(result, computationResultsDto, new DvhVolumeResult());

		dvhVolumeResult.setDoseSopUid(computationResultsDto.getSopUids().get(Computation.RTDOSE).get(0));

		dvhVolumeResult.setVolumeUnit(Boolean.valueOf(result.getResults().get(Computation.ABSOLUTE_OUTPUT)) ? "cc" : "%");
		dvhVolumeResult.setBoundary(result.getResults().get(Computation.LIMIT));
		dvhVolumeResult.setDoseUnit(result.getResults().get(Computation.LIMIT_UNIT));
		
		try{
			String resultFloat = result.getResults().get(Computation.RESULT);
			if(resultFloat != null){
				dvhVolumeResult.setResult(Float.parseFloat(resultFloat));
			}
		}
		catch(NumberFormatException e){
			LOGGER.warn("dvhVolumeResult result is not a Float for patientId:" + computationResultsDto.getPatientId());
		}

		return dvhVolumeResultRepository.save(dvhVolumeResult) != null;
	}

	private boolean saveVolumeComputation(ComputationResultDto result, ComputationResultsDto computationResultsDto){
		VolumeResult volumeResult = this.setGetBaseResultAttributes(result, computationResultsDto, new VolumeResult());

		volumeResult.setStructSopUid(computationResultsDto.getSopUids().get(Computation.RTSTRUCT).get(0));

		try{
			String resultFloat = result.getResults().get(Computation.RESULT);
			if(resultFloat != null){
				volumeResult.setResult(Float.parseFloat(resultFloat));
			}
		}
		catch(NumberFormatException e){
			LOGGER.warn("volumeResult result is not a Float patientId:" + computationResultsDto.getPatientId());
		}

		return volumeResultRepository.save(volumeResult) != null;
	}

	private <R extends GenericResult> R setGetBaseResultAttributes(
			ComputationResultDto result, ComputationResultsDto computationResultsDto, R genericResult){

		genericResult.setVersion(result.getVersion());
		genericResult.setCalculationIdentifier(result.getComputationIdentifier());
		genericResult.setPatientId(computationResultsDto.getPatientId());
		genericResult.setPlanLabel(computationResultsDto.getPlanLabel());
		genericResult.setContainerId(computationResultsDto.getContainerId());
		genericResult.setVolumeOfInterest(this.stringifyVolumesOfInterest(result.getVolumesOfInterest()));
		genericResult.setError(result.getError());

		return genericResult;
	}

	private String stringifyVolumesOfInterest(List<VolumeOfInterest> voiList){
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < voiList.size(); j++) {
			VolumeOfInterest voi = voiList.get(j);
			for (int i = 0; i < voi.getOperators().size(); i++){
				sb.append(voi.getRois().get(i)).append(voi.getOperators().get(i));
			}
			sb.append(voi.getRois().get(voi.getRois().size()-1));
			if (j < (voiList.size()-1)) {
				sb.append(", ");
			}
		}
		return sb.toString();
	}
}