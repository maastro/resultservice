package nl.maastro.mia.resultservice.service.rdf;

import org.springframework.stereotype.Service;

import nl.maastro.mia.resultservice.constants.PrefixDictionary;
import nl.maastro.mia.resultservice.constants.SparqlDictionary;
import nl.maastro.mia.resultservice.data.sparql.TripleElement;
import nl.maastro.mia.resultservice.web.dto.VolumeOfInterest;

@Service
public class ElementCreateService {

	public TripleElement getRegionOfInterestIdentifier(String roi) {
		return 	new TripleElement(PrefixDictionary.MIA.getName(),
				getSparqlFriendlyHashCode(roi));
	}

	public TripleElement getLabelPredicate() {
		return new TripleElement(PrefixDictionary.RDFS.getName(),
				SparqlDictionary.LABEL);
	}

	public TripleElement getVolumeOfInterestIdentifier(VolumeOfInterest volumeOfInterest) {
		return new TripleElement(PrefixDictionary.MIA.getName(),
				getSparqlFriendlyHashCode(volumeOfInterest.getName()));
	}

	public TripleElement getRoiPredicate() {
		return new TripleElement(PrefixDictionary.MIA.getName(),
				SparqlDictionary.HAS_ROI);
	}

	public TripleElement getRegionOfInterest(String roi) {
		return new TripleElement(PrefixDictionary.MIA.getName(),
				getSparqlFriendlyHashCode(roi));
	}

	public TripleElement getDataTypePredicate() {
		return new TripleElement(PrefixDictionary.RDFS.getName(),
				SparqlDictionary.DATATYPE);
	}

	public TripleElement getRadiationOncologyRegionOfInterest() {
		return new TripleElement(PrefixDictionary.ROO.getName(),
				SparqlDictionary.RADIATION_ONCOLOGY_REGION_OF_INTEREST);
	}

	public TripleElement getHasOperatorPredicate() {
		return new TripleElement(PrefixDictionary.MIA.getName(),
				SparqlDictionary.HAS_OPERATOR);
	}
	
	private String getSparqlFriendlyHashCode(Object hashable){
		String string = Integer.toString(hashable.hashCode());
		string = string.replaceAll("\\-", "m");
		return string;
	}
}