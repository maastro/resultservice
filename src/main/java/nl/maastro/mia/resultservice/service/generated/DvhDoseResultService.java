package nl.maastro.mia.resultservice.service.generated;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import nl.maastro.mia.resultservice.entity.DvhDoseResult;
import nl.maastro.mia.resultservice.repository.DvhDoseResultRepository;
import nl.maastro.mia.resultservice.web.dto.generated.DvhDoseResultDto;
import nl.maastro.mia.resultservice.web.mapper.DvhDoseResultMapper;

/**
 * Service Implementation for managing DvhDoseResult.
 */
@Service
@Transactional
public class DvhDoseResultService {

    private final Logger log = LoggerFactory.getLogger(DvhDoseResultService.class);
    
    @Autowired
    private DvhDoseResultRepository dvhDoseResultRepository;
    
    @Autowired
    private DvhDoseResultMapper dvhDoseResultMapper;

    /**
     *  Get all the dvhDoseResults.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<DvhDoseResult> findAll(Pageable pageable) {
        log.debug("Request to get all DvhDoseResults");
        Page<DvhDoseResult> result = dvhDoseResultRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one dvhDoseResult by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public DvhDoseResultDto findOne(Long id) {
        log.debug("Request to get DvhDoseResult : {}", id);
        DvhDoseResult dvhDoseResult = dvhDoseResultRepository.findOne(id);
        DvhDoseResultDto dvhDoseResultDTO = dvhDoseResultMapper.map(dvhDoseResult);
        return dvhDoseResultDTO;
    }

	public Page<DvhDoseResult> search(String query, Pageable pageable) {
		return dvhDoseResultRepository.findByPatientId(query, pageable);
	}

}
