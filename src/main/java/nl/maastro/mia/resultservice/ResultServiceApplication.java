package nl.maastro.mia.resultservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ResultServiceApplication { // NOSONAR

	public static void main(String[] args) {
		SpringApplication.run(ResultServiceApplication.class, args); // NOSONAR
	}
}
