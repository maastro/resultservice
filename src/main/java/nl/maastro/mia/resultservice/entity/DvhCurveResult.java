package nl.maastro.mia.resultservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class DvhCurveResult extends GenericResult{

	@Id
	@GeneratedValue
	private Long id;

	private String doseSopUid;
	
	private String volumeUnit;
	
	@Column(columnDefinition="text")
	private String doseVector;
	
	@Column(columnDefinition="text")
	private String volumeVector;
	
	public String getVolumeUnit() {
		return volumeUnit;
	}
	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}
	public String getDoseVector() {
		return doseVector;
	}
	public void setDoseVector(String doseVector) {
		this.doseVector = doseVector;
	}
	public String getVolumeVector() {
		return volumeVector;
	}
	public void setVolumeVector(String volumeVector) {
		this.volumeVector = volumeVector;
	}
	public String getDoseSopUid() {
		return doseSopUid;
	}
	public void setDoseSopUid(String doseSopUid) {
		this.doseSopUid = doseSopUid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}