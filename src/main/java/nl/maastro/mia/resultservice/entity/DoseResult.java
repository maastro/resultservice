package nl.maastro.mia.resultservice.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class DoseResult extends GenericResult{
	
	@Id
	@GeneratedValue
	private Long id;

	
	private String doseSopUid;
	
	private String operation;
	
	private Float result;
	
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getDoseSopUid() {
		return doseSopUid;
	}
	public void setDoseSopUid(String doseSopUid) {
		this.doseSopUid = doseSopUid;
	}
	public Float getResult() {
		return result;
	}
	public void setResult(Float result) {
		this.result = result;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}