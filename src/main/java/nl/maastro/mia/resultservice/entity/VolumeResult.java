package nl.maastro.mia.resultservice.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class VolumeResult extends GenericResult{

	@Id
	@GeneratedValue
	private Long id;
	
	private String structSopUid;
	
	private Float result;

	

	public String getStructSopUid() {
		return structSopUid;
	}

	public void setStructSopUid(String structSopUid) {
		this.structSopUid = structSopUid;
	}

	public Float getResult() {
		return result;
	}

	public void setResult(Float result) {
		this.result = result;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}