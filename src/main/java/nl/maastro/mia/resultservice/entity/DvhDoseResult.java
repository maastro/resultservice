package nl.maastro.mia.resultservice.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class DvhDoseResult extends GenericResult{

	@Id
	@GeneratedValue
	private Long id;

	private String doseSopUid;
	
	private String doseUnit;

	private String boundary;
	
	private String volumeUnit;
	
	private String targetPrescriptionDose;
	
	private Float result;
	

	public String getDoseSopUid() {
		return doseSopUid;
	}

	public void setDoseSopUid(String doseSopUid) {
		this.doseSopUid = doseSopUid;
	}

	public String getBoundary() {
		return boundary;
	}

	public void setBoundary(String boundary) {
		this.boundary = boundary;
	}

	public String getDoseUnit() {
		return doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	public String getVolumeUnit() {
		return volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public String getTargetPrescriptionDose() {
		return targetPrescriptionDose;
	}

	public void setTargetPrescriptionDose(String targetPrescriptionDose) {
		this.targetPrescriptionDose = targetPrescriptionDose;
	}

	public Float getResult() {
		return result;
	}

	public void setResult(Float result) {
		this.result = result;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

}