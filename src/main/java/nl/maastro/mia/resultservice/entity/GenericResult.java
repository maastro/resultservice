package nl.maastro.mia.resultservice.entity;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class GenericResult {

	private String version;
	
	private String calculationIdentifier;
	
	private String patientId;
	
	private String planLabel;
	
	private String containerId;
	
	private String volumeOfInterest;
	
	private String error;
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getCalculationIdentifier() {
		return calculationIdentifier;
	}
	public void setCalculationIdentifier(String calculationIdentifier) {
		this.calculationIdentifier = calculationIdentifier;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getPlanLabel() {
		return planLabel;
	}
	public void setPlanLabel(String planLabel) {
		this.planLabel = planLabel;
	}
	public String getContainerId() {
		return containerId;
	}
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}
	public String getVolumeOfInterest() {
		return volumeOfInterest;
	}
	public void setVolumeOfInterest(String volumeOfInterest) {
		this.volumeOfInterest = volumeOfInterest;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
}