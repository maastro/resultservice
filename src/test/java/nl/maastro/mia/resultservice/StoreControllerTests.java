package nl.maastro.mia.resultservice;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.maastro.mia.resultservice.config.RdfConfiguration;
import nl.maastro.mia.resultservice.web.controller.StoreController;
import nl.maastro.mia.resultservice.web.dto.ComputationResultsDto;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class StoreControllerTests {
	
	private static final String TEST_RESULTS_JSON = "testResultsDto.json";
	private static ComputationResultsDto testResultsDto;
	
	@Autowired
	private RdfConfiguration rdfConfiguration;

	@Autowired
	private StoreController storeController;
	
	@BeforeClass
	public static void setup() throws IOException {
		ClassLoader classLoader = StoreControllerTests.class.getClassLoader();
		File file = new File(classLoader.getResource(TEST_RESULTS_JSON).getFile());
		String testResultsJson = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
		testResultsDto = new ObjectMapper().readValue(testResultsJson, ComputationResultsDto.class);
	}
	
	@Test
	public void testStoreToSql() {
		rdfConfiguration.setEnabled(false);
		ResponseEntity<Boolean> response = storeController.storeCalculationResult(testResultsDto, null);
		assertTrue(response.getBody());
	}
	
	@Test
	@Ignore // needs running sparql endpoint
	public void testStoreToSparql() throws IOException {
		rdfConfiguration.setEnabled(true);
		ResponseEntity<Boolean> response = storeController.storeCalculationResult(testResultsDto, null);
		assertTrue(response.getBody());
	}
}
