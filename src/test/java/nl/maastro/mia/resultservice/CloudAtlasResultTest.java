package nl.maastro.mia.resultservice;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.maastro.mia.resultservice.web.controller.StoreController;
import nl.maastro.mia.resultservice.web.dto.ComputationResultsDto;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class CloudAtlasResultTest {
	
	private static final Logger LOGGER = Logger.getLogger(CloudAtlasResultTest.class);
	@Autowired
	StoreController storeController;
		
	@Test
	@Ignore //needs running sparql endpoint
	public void dvhCloudAtlasTest() throws IOException{
		LOGGER.info("reading container json...");
		String src = new String(Files.readAllBytes(Paths.get("\\\\dev-build.maastro.nl\\testdata\\CloudAtlas\\JSON\\MIA-196-resultsDto-v2.txt")));
		LOGGER.info("mapping container json to dto");
		ComputationResultsDto computationResultsDto = new ObjectMapper().readValue(src, ComputationResultsDto.class);
		LOGGER.info("store dto");

		ResponseEntity<Boolean> response = storeController.storeCalculationResult(computationResultsDto, null);
		assertTrue(response.getBody());
		
	}

}
