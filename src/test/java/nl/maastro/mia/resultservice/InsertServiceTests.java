package nl.maastro.mia.resultservice;

import static org.junit.Assert.assertTrue;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import nl.maastro.mia.resultservice.service.SparqlInsertService;
import nl.maastro.mia.resultservice.service.SqlInsertService;
import nl.maastro.mia.resultservice.web.dto.ComputationResultDto;
import nl.maastro.mia.resultservice.web.dto.ComputationResultsDto;
import nl.maastro.mia.resultservice.web.dto.VolumeOfInterest;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class InsertServiceTests {
	
	private static ComputationResultsDto testResultsDto;
	
	@Autowired
	private SqlInsertService sqlInsertService;
	
	@Autowired
	private SparqlInsertService sparqlInsertService;
	
	@BeforeClass
	public static void setup() {
		testResultsDto = new ComputationResultsDto();

		testResultsDto.setPatientId("12345");
		Map<String, List<String>> sopUids = new HashMap<>();
		sopUids.put("RTDOSE", Arrays.asList(new String[]{"1.2.3.4.12312312453463623"}));
		testResultsDto.setSopUids(sopUids);
		
		VolumeOfInterest voi = new VolumeOfInterest();
		voi.setName("someVoi");
		List<String> rois = new ArrayList<>();
		rois.add("Linkerlong");
		rois.add("Pelvis");
		rois.add("Eso");
		voi.setRois(rois);
		List<String> operators = new ArrayList<>();
		operators.add("+");
		operators.add("-");
		voi.setOperators(operators);
		
		ComputationResultDto resultDto1 = new ComputationResultDto();
		resultDto1.setComputationIdentifier("dose");
		resultDto1.setModuleName("DoseComputation");
		Map<String,String> results = new HashMap<>();
		results.put("result", "9000.045");
		results.put("doseOperation", "mean");
		resultDto1.setResults(results);
		resultDto1.setVersion("1.1.0");
		List<VolumeOfInterest> voiList = new ArrayList<VolumeOfInterest>();
		voiList.add(voi);
		resultDto1.setVolumesOfInterest(voiList);
		
		ComputationResultDto resultDto2 = new ComputationResultDto();
		resultDto2.setComputationIdentifier("dose");
		resultDto2.setModuleName("DoseComputation");
		Map<String,String> results2 = new HashMap<>();
		results2.put("result", "11500.022");
		results2.put("doseOperation", "max");
		resultDto2.setResults(results2);
		resultDto2.setVersion("1.1.0");
		resultDto2.setVolumesOfInterest(voiList);
		
		List<ComputationResultDto> compResults = Arrays.asList(resultDto1, resultDto2);
		testResultsDto.setComputationResults(compResults);
	}
	
	@Test
	public void testSqlInsertService() throws URISyntaxException{
		boolean success = sqlInsertService.insertResults(testResultsDto);
		assertTrue(success);
	}
	
	@Test
	@Ignore // needs running sparql endpoint
	public void testSparqlInsertService() throws URISyntaxException{
		boolean success = sparqlInsertService.insertResults(testResultsDto);
		assertTrue(success);
	}
}
