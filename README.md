# MIA Result Service #
 
The ResultService can be used to export [MIA framework](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Home). worker results to a database. Currently, exporting results is supported for Volume, Dose and DVH calculations.

## Prerequisites ##

- Java 8
- 512 MB RAM
- Modern browser
- SPARQL endpoint (optional)
 
 ## Usage ##

 The [swagger interface](http://localhost:8200/swagger-ui.html) will provide a summary of all possible functions and requests.

## SQL ##

For SQL the options are MySql, PostgreSql and MsSql (and h2). These are configured using the environment system variables. The database results are currently 5 tables, one table per type of computation:

### Tables ###

Table: Generic (not a table on its own)
Discription: All results tables extend this table (and therefore contain the following columns)

| Column Name           | Java Type     | Db Type       | Description                                                   
| ---                   | ---           | ---           | ---                                               
| version               | String        |               |  Jar build version of MIA Worker which computated the results
| calculationIdentifier | String        |               | Name of computation, given by user
| patientId             | String        |               | Patient Id
| planLabel             | String        |               | Plan Label
| containerId           | String        |               | Container Id, created in containerService
| volumeOfInterest      | String        |               | The computation is made on a structure or combination of structures; the volume of interest (VOI)
| error                 | String        |               | Error description if an error occurred, otherwise NULL
  
Table: VolumeResult
Description: Volume of the VOI in cc

| Column Name           | Java Type     | Db Type       | Enum              | Description
| ---                   | ---           | ---           | ---               | ---
| id                    | Long          | BigInt        |                   | database row id
| structSopUid          | String        |               |                   | SopUid of Dicom Struct file (RTStruct)
| result                | Float         |               |                   | Volume as float (always in cc)

Table: DoseResult
Description: Dose (min,mean,max) given to the VOI in Gy

| Column Name           | Java Type     | Db Type       | Enum              | Description                                                   
| ---                   | ---           | ---           | ---               | ---                          
| id                    | Long          | BigInt        |                   | database row id
| doseSopUid            | String        |               |                   | SopUid of Dicom Dose file (RTDose)
| operation             | String        |               | min, max, mean    | Type of dose computed
| result                | Float         |               |                   | Dose as float (always in Gy)
 
DVH Tables

Table: DvhVolumeResult 
Description: DVH-Vx parameter for a selected VOI (volume in which the dose exceeds a specified limit).

| Column Name           | Java Type     | Db Type       | Enum              | Description                                                   
| ---                   | ---           | ---           | ---               | ---                   
| id                    | Long          | BigInt        |                   | database row id
| doseSopUid            | String        |               |                   | SopUid of Dicom Dose File (RTDose)
| boundary              | String        |               |                   | Dose limit (in Gray or in percent of the target prescription dose)
| doseUnit              | String        | Gy, %         |                   | Unit of the dose limit 
| result                | Float         |               |                   | DVH-Vx result as float (in cc or in percent of the total VOI volume)
| volumeUnit            | String        | cc, %         |                   | Unit of DVH-Vx result 

Table: DvhDoseResult 
Description: DVH-Dx parameter for a selected VOI (minimum dose delivered to x% of the volume).

| Column Name           | Java Type     | Db Type       | Enum              | Description                                                   
| ---                   | ---           | ---           | ---               |  ---                  
| id                    | Long          | BigInt        |                   | database row id
| doseSopUid            | String        |               |                   | SopUid of Dicom Dose File (RTDose)
| limit                 | String        |               |                   | Volume limit (in cc or in percent of the total VOI volume) 
| volumeUnit            | String        | cc, %         |                   | Unit of the volume limit
| targetPrescriptionDose| String        |               |                   | The target prescription dose (from the RTPLAN)
| result                | Float         |               |                   | DVH-Dx result as float (in Gray or in percent of the target prescription dose)
| doseUnit              | String        | Gy, %         |                   | Unit of DVH-Dx result
 
Table: DvhCurveResult 
Description: Dvh curve for the selected VOI, where the volume can be in cc or %, and the dose is always in Gy

| Column Name           | Java Type     | Db Type       | Enum              | Description                                                   
| ---                   | ---           | ---           | ---               | ---                          
| id                    | Long          | BigInt        |                   | database row id
| doseSopUid            | String        |               |                   | SopUid of Dicom Dose File (RTDose)
| binSize               | Float         |               |                   | Stepsize
| volumeUnit            | String        |               | cc, %             | Unit of the volumeVector in cc or % (absolute or relative)
| doseVector            | String        |               |                   | Dose Vector, corresponding with the stepSize
| volumeVector          | String        |               |                   | Volume Vector, corresponding with the stepSize


## RDF (SPARQL) ###

For a SPARQL export a SPARQL endpoint such as [Blazegraph](https://www.blazegraph.com/) is required.
The following properties have to be configured in the application.yml:

```yaml
    rdf:
      queryfolder: ./sparqlqueries
      #set to true to export to sparql, false to export to sql
      enabled: false
      endpoint: http://localhost:9999/blazegraph/namespace/kb/sparql
```

**queryfolder:** For RDF the export a folder can be configured which will hold a sparql query .ttl file for each module to be exported. The name of the file should be equal to the name of the computationservice to export results from. The computationservice in a worker can put specific results in the results map under a desired key. This key can then be used to insert the result in the sparql query by encasing it in two hashtags on either side: ##examplekey##. Furthermore, the default results over all computationmodules can be specified in the same way (e.g. patientid, version). The volume of interest used will also be inserted into sparql by default, only the name of the voi needs to be specified.

**endpoint:** The SPARQL endpoint, the default configuration expects a local running blazegraph instance

### DvhCurve.ttl example ###

```sparql
	PREFIX mia:<http://cancerdata.org/mia/miaData#>
	PREFIX miaversion:<http://cancerdata.org/mia/miaVersion#>
	PREFIX sedi:<http://semantic-dicom.org/dcm#>
	PREFIX uo:<http://http://purl.obolibrary.org/obo/UO_>
	PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
	PREFIX ncit:<http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#>
	PREFIX roo:<http://www.cancerdata.org/roo#>
	PREFIX owl:<http://www.w3.org/2002/07/owl#>
	
	INSERT {
	sedi:##RTDOSE## sedi:hasInstanceUID "##RTDOSE##"^^xsd:string.
	mia:##UUID## mia:basedOnImage sedi:##RTDOSE##.

	mia:##PATIENTID## rdf:label "##PATIENTID##"^^xsd:string.
	mia:##PATIENTID## roo:100214 mia:##UUID##.
		
	mia:##UUID## rdf:type ncit:C112816.
	mia:##UUID## roo:100267 miaversion:##VERSION##.

	mia:##UUID## mia:volumevector "##volumevector##"^^xsd:string.
	mia:##UUID## mia:dosevector "##dosevector##"^^xsd:string.
	mia:##UUID## mia:absoluteOutput "##isAbsolute##".
	mia:##UUID## mia:binsize "##binsize##".
	
	mia:##UUID## mia:error "##ERROR##".
	
	mia:##UUID## mia:basedOnVolumeOfInterest  ?voi.	
	}
	WHERE { 
		?voi rdfs:label "##VOLUMEOFINTEREST##"@local
	}
```

### Blazegraph Configuration ###

Blazegraph configuration can be found in "./src/main/resources/sparqlqueries" (unarchive the jar). This configuration will prevent file locking issues under Windows.